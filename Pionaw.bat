@echo off

color f

set SW=59
set SH=49
set SP=3
set /a SW1=SW-1,SW_2=SW/2,SH_2=SH/2

rem 更改代码页(针对旧版控制台)
chcp 437
rem 更改字体大小
tool\FontSize 8 8
rem 更改控制台大小
mode con cols=59 lines=59
rem 更改标题
title Pionaw
rem 启用 变量拓展延迟 (!xx!)
rem 注意:setlocal enabledelayedexpansion不是任何时候都可以加的
setlocal enabledelayedexpansion

:title
color 0F
cls
type res\title.txt
call tool\getkey.bat
if %return% equ 1 goto load
if %return% equ 2 (
	for /l %%a in (1,1,4) do (
		cls
		type res\help_%%a.txt
		pause
	)
)
if %return% equ 3 exit /b

goto title

:load
if exist res\first.run call :first_run

for /f "tokens=1 delims==" %%a in ('set S_ 2^>nul') do set %%a=

rem 获取sin函数表
call tool\sintable.bat
rem 生成迷宫
call tool\generate.bat
rem 获取数字
call tool\numtable.bat

rem 获取迷宫大小
for /f "tokens=1-2" %%x in (size.txt) do (
	set M_x=%%x
	set M_y=%%y
)
set /a Mx1=M_x-1,My1=M_y-1

rem 读取迷宫(M的下标从0开始,坐标轴y反向(向上为正方向))
set y=%My1%
for /f %%l in (map.txt) do (
	set M_!y!=%%l
	set /a y-=1
)
rem 迷宫的一个格子有100个单位

rem 1.5个格子
set P_x=150
rem 1.5个格子
set P_y=150
rem 初始角度(向右下)
set P_a=130
rem hope
set P_l=100
rem destroy
set P_k=0
rem 是否击杀三角形
set B_SHOOT=0
rem 击杀三角形冷却时间
set CD_SHOOT=0
rem 三角形扎伤冷却时间
set CD_SPIKE=0

rem 启动控制窗口
call tool\ctrl2.bat press.txt

rem 播放背景音乐
cd .>Res\bgm.is
start /b cmd /c tool\playbgm.bat

rem 开始游戏
call tool\engine.bat

set ret=%errorlevel%

rem 停止播放背景音乐
del press.txt 2>nul
del Res\bgm.is
taskkill /im gplay.exe /f >nul 2>nul

if %ret% equ 1 call :wue
if %ret% equ 2 call :die
color 0F
goto title

:first_run
cls
type res\guide_1.txt
ping -n 4 0 >nul

set t1=%time%
set d1=1

:guide2
cls
type res\guide_2_%d1%.txt
set /a d1=3-d1

set t2=%time%
call :timediff
if %t% lss 100 goto guide2

for /l %%a in (1,1,4) do (
	cls
	type res\help_%%a.txt
	pause
)

del res\first.run

goto :eof

:die
start /b tool\gplay.exe res\die.wav >nul 2>nul
ping -n 1 0 >nul

set d1=1
set t1=%time%

:die1
cls
type res\die_1_%d1%.txt
set /a d1=3-d1
set t2=%time%
call :timediff
set /a c=(t/50)%%2
if %c% equ 0 (color 4F) else color 0F
if %t% lss 200 goto die1

:die2
color 4F

set t2=%time%
call :timediff
set /a k=t-200

set SC_0=
for /l %%x in (1,1,%SW%) do set "SC_0=!SC_0! "
for /l %%y in (1,1,60) do set SC_%%y=!SC_0!
set /a "x1=SW_2-SW_2*k/100,x2=SW_2+SW_2*k/100,y=30+30*k/100

for /l %%x in (!x1!,1,!x2!) do (
	set /a x0=%%x+1
	if %%x leq !SW_2! (
		set /a "y1=y-(%%x-x1)*2"
	) else (
		set /a "y1=y-(x2-%%x)*2"
	)
	set /a y2=y1-1
	for /f "tokens=1-2" %%y in ("!y! !x0!") do (
		set "SC_%%y=!SC_%%y:~0,%%x!_!SC_%%y:~%%z!"
	)
	for %%z in (!x0!) do (
		for /l %%y in (!y2!,1,!y1!) do (
			set "SC_%%y=!SC_%%y:~0,%%x!|!SC_%%y:~%%z!"
		)
	)
)

set SC=
for /l %%y in (0,1,60) do set SC=!SC!!SC_%%y!
cls & echo(!SC!
if %t% lss 300 goto die2

cls
set d3=1
set d3l=9ABCDE
:die3
set /a d3=(d3+1)%%6
color !d3l:~%d3%,1!0
set t2=%time%
call :timediff
if %t% lss 350 goto die3

cls
type res\die_3.txt
:die4
for %%c in (4 C 6 E) do color %%c0
set t2=%time%
call :timediff
if %t% lss 500 goto die4

cls
color E0
ping -n 1 0 >nul
color 0F
ping -n 1 0 >nul
color E0
ping -n 1 0 >nul
color 0F
ping -n 3 0 >nul

cls
color 06
type res\die.txt
echo;
echo;
echo;
echo;
echo;
echo;
echo;
echo;
echo;
echo;___________________________________________________________
echo;
echo;
echo               YOU DESTROYED %p_k% TRIANGLES^^!
ECHO;
ECHO;
ECHO                 GOOD LUCK NEXT TIME^^!

ping -n 1 0 >nul
color 46
ping -n 1 0 >nul
color 4E
ping -n 1 0 >nul
color CE

pause>nul
goto :eof

:wue
start /b tool\gplay.exe res\wue.wav >nul 2>nul
ping -n 1 0 >nul

set t1=%time%

cls
type res\wue_1.txt
ping -n 2 0 >nul

set d1=1
cls
type res\wue_2.txt
:wue2
set /a d1=3-d1
set t2=%time%
call :timediff
if %d1% equ 1 (color A0) else color B0
if %t% lss 300 goto wue2

cls
type res\wue_3.txt
:wue3
set /a d1=3-d1
set t2=%time%
call :timediff
if %d1% equ 1 (color C0) else color E0
if %t% lss 500 goto wue3

cls
color B0
ping -n 1 0 >nul
color 0F
ping -n 1 0 >nul
color B0
ping -n 1 0 >nul
color 0F
ping -n 3 0 >nul

color 03
type res\WUE.txt

echo;
echo;
echo;
echo;
echo;
echo;
echo;
echo;
echo;
echo;___________________________________________________________
echo;
echo;
echo                 YOU DESTROYED 317 TRIANGLES^^!

ping -n 1 0 >nul
color 13
ping -n 1 0 >nul
color 1B
ping -n 1 0 >nul
color 9B

pause>nul
goto :eof

:timediff
rem http://bbs.bathome.net/thread-4701-1-1.html
set /a "t=1!t2:~-5,2!!t2:~-2!-1!t1:~-5,2!!t1:~-2!,t+=-6000*(t>>31)"
goto :eof