			Pionaw


		丧心病狂的生存小游戏

			Oh，yeah~

			2018-12-09

遵循：https://creativecommons.org/licenses/by/3.0/cn/ 协议


		操作方法: AWSD移动, JL转向, K射击

		目标: 躲避并击杀场景中的三角形

			击杀317个即为胜利



	提示:

		1：注意三角形, 防不胜防!

		2：三角形会从你的后面出现，要小心。

		3：按 A 或 D 键是左右平移，J 和 L 键才是转向。

		4：听说呆在墙角能够骗分。

		5：如果这堆bat在你的机器上出了问题，请到
			https://gcript.ml/2018-Pionaw/
		或
			https://github.com/gcript/pionaw/issues/new
		提出反馈。 感谢您的支持。

		6: 祝各位好运。

						Authors:
							gcript (@gcript)
							gtr-0000 (@_greater_)