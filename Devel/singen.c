
#include <stdio.h>
#include <math.h>

const double pi = 3.141592653589793;

int main(int argc, char *argv[])
{
	int	a;

	//生成sin函数表 (注：Zsin角度 = sin(角度) × 100)
	freopen("sintable.bat", "w", stdout);
	printf("set /a Zsin0=0");
	for(a = 10; a < 360; a += 10) printf(",Zsin%d=%d", a, (int) (sin(a * pi / 180) * 100));

	//顺便生成cos函数表
	for(a = 0; a < 360; a += 10) printf(",Zcos%d=%d", a, (int) (cos(a * pi / 180) * 100));
	return 0;
}
