for /l %%k in (1,1,20) do (
	set SC_0=
	for /l %%x in (1,1,%SW%) do set "SC_0=!SC_0! "
	for /l %%y in (1,1,60) do set SC_%%y=!SC_0!
	set /a "x1=SW_2-SW_2*%%k/20,x2=SW_2+SW_2*%%k/20,y=30+30*%%k/20,x=(x1+x2)/2"
	for /l %%x in (!x1!,1,!x2!) do (
		set /a x0=%%x+1
		if %%x leq !x! (
			set /a "y1=y-(%%x-x1)*2"
		) else (
			set /a "y1=y-(x2-%%x)*2"
		)
		set /a y2=y1-1
		for /f "tokens=1-2" %%y in ("!y! !x0!") do (
			set "SC_%%y=!SC_%%y:~0,%%x!_!SC_%%y:~%%z!"
		)
		for %%z in (!x0!) do (
			for /l %%y in (!y2!,1,!y1!) do (
				set "SC_%%y=!SC_%%y:~0,%%x!|!SC_%%y:~%%z!"
			)
		)
	)
	set SC=
	for /l %%y in (0,1,60) do set SC=!SC!!SC_%%y!
	cls & echo(!SC!
	for /l %%a in (0,1,100) do break
)