@echo off
setlocal enabledelayedexpansion

rem 将下面的'REM'去掉以观察地图生成过程
set DEBUG=REM

set Mx=15
set My=15

set /a Mx1=Mx-1,Mx2=Mx-2,My1=My-1,My2=My-2,My0=My+1

%DEBUG% fontsize 1 1
%DEBUG% mode %Mx%,%My0%

set M_0=
for /l %%x in (1,1,%Mx%) do set M_0=!M_0!X
set M_%My1%=!M_0!

set M_1=
for /l %%x in (1,1,%Mx2%) do set M_1=!M_1!.
set M_1=X!M_1!X
for /l %%y in (2,1,%My2%) do set M_%%y=!M_1!

call :xgen 0 0 %Mx1% %My1%

(echo %Mx% %My%)>size.txt
(for /l %%y in (0,1,%My1%) do echo !M_%%y!)>map.txt

%DEBUG% pause

exit /b

:xgen
set /a x1=%1,y1=%2,x2=%3,y2=%4
set /a xV=x2-x1
if %xV% leq 2 goto :eof
set /a yV=y2-y1
if %yV% leq 2 goto :eof

set /a x0=x1+2+!random!%%((x2-x1-2)/2)*2
set /a y01=y1+1+!random!%%((y2-y1-2)/2)*2
set /a y02=y1+1+!random!%%((y2-y1-2)/2)*2

set /a z=x0+1

for /l %%y in (%y1%,1,%y2%) do if %%y neq %y01% if %%y neq %y02% (
	set M_%%y=!M_%%y:~0,%x0%!X!M_%%y:~%z%!
)

%DEBUG% cls
%DEBUG% for /l %%y in (0,1,%My1%) do echo !M_%%y!

call :ygen %x1% %y1% %x0% %y2% & call :ygen %x0% %y1% %x2% %y2%
goto :eof

:ygen
set /a x1=%1,y1=%2,x2=%3,y2=%4
set /a yV=y2-y1
if %yV% leq 2 goto :eof
set /a xV=x2-x1
if %xV% leq 2 goto :eof

set /a y0=y1+2+!random!%%((y2-y1-2)/2)*2
set /a x01=x1+1+!random!%%((x2-x1-2)/2)*2
set /a x02=x1+1+!random!%%((x2-x1-2)/2)*2

for /l %%x in (%x1%,1,%x2%) do if %%x neq %x01% if %%x neq %x02% (
	set /a z=%%x+1
	for %%z in (!z!) do set M_%y0%=!M_%y0%:~0,%%x!X!M_%y0%:~%%z!
)

%DEBUG% cls
%DEBUG% for /l %%y in (0,1,%My1%) do echo !M_%%y!

call :xgen %x1% %y1% %x2% %y0% & call :xgen %x1% %y0% %x2% %y2%

goto :eof
)