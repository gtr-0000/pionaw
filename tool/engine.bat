:loop

:: 玩家操作 ::
set P_sin=!Zsin%P_a%!
set P_cos=!Zcos%P_a%!

rem 获取按键
rem 关闭控制窗口,返回0
if not exist press.txt exit /b 0
set press=
set /p press=<press.txt

rem 玩家移动量
set sx=0
set sy=0

set B_SHOOT=0
for %%p in (%press%) do (
	rem J左转
	if %%p equ 74 set /a P_a-=10
	rem L右转
	if %%p equ 76 set /a P_a+=10
	rem A左
	if %%p equ 65 set /a sx=-P_cos/5,sy=-P_sin/5
	rem D右
	if %%p equ 68 set /a sx=+P_cos/5,sy=+P_sin/5
	rem W上
	if %%p equ 87 set /a sx=+P_sin/5,sy=-P_cos/5
	rem S下
	if %%p equ 83 set /a sx=-P_sin/5,sy=+P_cos/5
	rem K击杀钉子
	if %%p equ 75 if %CD_SPIKE% equ 0 set B_SHOOT=1
	rem ESC,返回0
	if %%p equ 27 exit /b 0
)
rem 角度转一圈调整
if %P_a% lss 0 set /a P_a+=360
if %P_a% geq 360 set /a P_a-=360

if %CD_SPIKE% gtr 1 set /a sx=S_sx,sy=S_sy

:: 撞墙判定 ::
set /a x0=P_x,y0=P_y

rem 向左,右移动

set /a P_x+=sx

rem 检测人能碰到的格子

rem 人的坐标距离墙应有20个单位的距离(一个格子100单位)

set /a "x1=(P_x-20)/100,x2=(P_x+20)/100,y1=(P_y-20)/100,y2=(P_y+20)/100"
set inWall=0
for /l %%x in (%x1%,1,%x2%) do (
	for /l %%y in (%y1%,1,%y2%) do (
		if "!M_%%y:~%%x,1!"=="X" set inWall=1
	)
)

rem 如果人碰到格子就适当回退

if %inWall% equ 1 (
	if %sx% lss 0 set /a "P_x=(x1+1)*100+21"
	if %sx% gtr 0 set /a "P_x=x2*100-21"
)

rem 特别地生成三角形移动导向标记
if %sx% gtr 0 (set "ch=>") else set "ch=<"
set /a x=x0/100,y=y0/100,z=x+1
set M_%y%=!M_%y%:~0,%x%!!ch!!M_%y%:~%z%!

rem 向上,下移动

set /a P_y+=sy

set /a "x1=(P_x-20)/100,x2=(P_x+20)/100,y1=(P_y-20)/100,y2=(P_y+20)/100"
set inWall=0
for /l %%x in (%x1%,1,%x2%) do (
	for /l %%y in (%y1%,1,%y2%) do (
		if "!M_%%y:~%%x,1!"=="X" set inWall=1
	)
)

if %inWall% equ 1 (
	if %sy% lss 0 set /a "P_y=(y1+1)*100+21"
	if %sy% gtr 0 set /a "P_y=y2*100-21"
)

:: 生成三角形移动导向标记 ::
set /a x=P_x/100,y=P_y/100
set Wall=0
rem 左边
for /l %%x in (%x%,-1,0) do if !Wall! equ 0 (
	if "!M_%y%:~%%x,1!"=="X" (
		set Wall=1
	) else (
		set /a z=%%x+1
		for %%z in (!z!) do (
			set "M_%y%=!M_%y%:~0,%%x!>!M_%y%:~%%z!"
		)
	)
)
rem 右边
set Wall=0
for /l %%x in (%x%,1,%Mx1%) do if !Wall! equ 0 (
	if "!M_%y%:~%%x,1!"=="X" (
		set Wall=1
	) else (
		set /a z=%%x+1
		for %%z in (!z!) do (
			set "M_%y%=!M_%y%:~0,%%x!<!M_%y%:~%%z!"
		)
	)
)
rem 上边
set Wall=0
set /a z=x+1
for /l %%y in (%y%,-1,0) do if !Wall! equ 0 (
	if "!M_%%y:~%x%,1!"=="X" (
		set Wall=1
	) else (
		set "M_%%y=!M_%%y:~0,%x%!V!M_%%y:~%z%!"
	)
)
rem 下边
set Wall=0
for /l %%y in (%y%,1,%My1%) do if !Wall! equ 0 (
	if "!M_%%y:~%x%,1!"=="X" (
		set Wall=1
	) else (
		set "M_%%y=!M_%%y:~0,%x%!A!M_%%y:~%z%!"
	)
)

:: 生成三角形 ::
set s=0
rem 检测可用的三角形编号
for /l %%s in (20,-1,1) do if not defined S_%%s_x set s=%%s

if %s% neq 0 (
	rem 新建三角形
	set /a x0=!random! %% M_x*100+50,y0=!random! %% M_y*100+50
	set /a x=x0/100,y=y0/100
	for /f "tokens=1-2" %%x in ("!x! !y!") do (
		rem                  %%x %%y
		if "!M_%%y:~%%x,1!" neq "X" (
			set /a "S_!s!_x=x0,S_!s!_y=y0,S_!s!_Sx=0,S_!s!_Sy=-15"
		)
	)
)

:: 三角形判定 ::
if %CD_SPIKE% gtr 0 (
	set /a CD_SPIKE-=1
) else (
	rem WUE,返回1
	if !P_k! equ 317 exit /b 1
	rem 扎穿了,返回2
	if !P_l! equ 0 exit /b 2
	for /l %%s in (1,1,20) do if defined S_%%s_x (
		rem 原坐标
		set /a x0=S_%%s_x,y0=S_%%s_y

		rem 检测移动标记
		set /a x=S_%%s_x/100,y=S_%%s_y/100
		for /f "tokens=1-2" %%x in ("!x! !y!") do (
			rem                  %%x %%y
			if "!M_%%y:~%%x,1!"=="<" set /a S_%%s_Sx=-15,S_%%s_Sy=0
			if "!M_%%y:~%%x,1!"==">" set /a S_%%s_Sx=15,S_%%s_Sy=0
			if "!M_%%y:~%%x,1!"=="A" set /a S_%%s_Sx=0,S_%%s_Sy=-15
			if "!M_%%y:~%%x,1!"=="V" set /a S_%%s_Sx=0,S_%%s_Sy=15
		)

		rem 如果在人附近则向人移动
		set /a xx=S_%%s_x/100-P_x/100
		set /a yy=S_%%s_y/100-P_y/100

		if !xx! equ 0 if !yy! equ 0 (
			if !S_%%s_x! gtr !P_x! set S_%%s_Sx=-15
			if !S_%%s_x! lss !P_x! set S_%%s_Sx=15
			if !S_%%s_y! gtr !P_y! set S_%%s_Sy=-15
			if !S_%%s_y! lss !P_y! set S_%%s_Sy=15
		)

		set /a S_%%s_x+=S_%%s_Sx,S_%%s_y+=S_%%s_Sy

		rem 检测三角形能碰到的格子
		set /a "x=S_%%s_x/100,y=S_%%s_x/100"

		set inWall=0

		for /f "tokens=1-2" %%x in ("!x!,!y!") do (
			if "!M_%%y:~%%x,1!"=="X" set inWall=1
		)

		set isTurn=0

		rem 三角形撞墙
		if !inWall! equ 1 (
			set /a S_%%s_x=x0,S_%%s_y=y0
			set isTurn=1
		) else (
			set /a x_=S_%%s_x %% 100,y_=S_%%s_y %% 100
			if !x_! lss 50 set /a S_%%s_x+=5
			if !x_! gtr 50 set /a S_%%s_x-=5
			if !y_! lss 50 set /a S_%%s_y+=5
			if !y_! gtr 50 set /a S_%%s_y-=5
		)

		if !random! lss 4096 set isTurn=1

		if !isTurn! equ 1 (
			rem 随机左/右转
			if !random! lss 16384 (
				set /a sx=S_%%s_Sy,sy=S_%%s_Sx
				set /a S_%%s_Sx=sx,S_%%s_Sy=sy
			) else (
				set /a sx=-S_%%s_Sy,sy=-S_%%s_Sx
				set /a S_%%s_Sx=sx,S_%%s_Sy=sy
			)
		)

		rem 扎伤人
		set /a x=S_%%s_x-P_x,y=S_%%s_y-P_y
		if !x! lss 40 if !x! gtr -40 (
			if !y! lss 40 if !y! gtr -40 if !CD_SPIKE! equ 0 (
				start /b tool\gplay res\Injure.wav >nul 2>nul
				set CD_SPIKE=5
				set /a S_%%s_x=x0,S_%%s_y=y0
				rem 击退
				set /a "S_Sx=(P_x-S_%%s_x)/5,S_Sy=(P_y-S_%%s_y)/5"
				set /a P_l-=!random!%%7+5
				if !P_l! lss 0 set P_l=0
			)
		)
	)
)

:: 击杀三角形 ::
if %B_SHOOT% equ 1 if %CD_SHOOT% equ 0 (
	rem 模拟激光发射(短距离)
	set inWall=0
	for /l %%k in (0,1,10) do if !inWall! equ 0 (
		set /a x=P_x+P_sin*%%k/2,y=P_y-P_cos*%%k/2
		set /a x1=x/100,y1=y/100
		rem 不能穿墙
		for /f "tokens=1-2" %%x in ("!x1! !y1!") do (
			rem                  %%x  %%y
			if "!M_%%y:~%%x,1!"=="X" set inWall=1
		)
		for /l %%s in (1,1,20) do if defined S_%%s_x (
			set /a x0=x-S_%%s_x,y0=y-S_%%s_y
			rem 击中
			if !x0! lss 25 if !x0! gtr -25 (
				if !y0! lss 25 if !y0! gtr -25 (
					start /b tool\gplay res\Break.wav >nul 2>nul
					set B_SHOOT=-1
					set /a P_k+=1
					if !P_k! gtr 317 set P_k=317
					set S_%%s_x=
					set S_%%s_y=
					set S_%%s_Sx=
					set S_%%s_Sy=
				)
			)
		)
	)
	set CD_SHOOT=2
)
if !CD_SHOOT! gtr 0 set /a CD_SHOOT-=1

:: 显示地图 ::

rem 初始化

for /l %%x in (0,1,%SW1%) do (
	rem 标记图像下半部分每一列最底下边的纵坐标
	set /a SCY_%%x=0
	rem 绘制模式 "-" 或 "|", "."表示不用绘制
	set SCC_%%x=.
)

rem 获取需要显示的边

set /a x0=P_x/100,y0=P_y/100
set /a x1=x0-SP,x2=x0+SP,y1=y0-SP,y2=y0+SP

if %x1% lss 0 set x1=0
if %x2% gtr %Mx1% set x2=%Mx1%
if %y1% lss 0 set y1=0
if %y2% gtr %My1% set y2=%My1%

set fs=
for /l %%x in (%x1%,1,%x2%) do (
	for /l %%y in (%y1%,1,%y2%) do (
		if "!M_%%y:~%%x,1!"=="X" (
			set /a fx1=%%x*100-P_x,fx2=fx1+100
			set /a fy1=%%y*100-P_y,fy2=fy1+100
			if !fx1! gtr 0 (
				set /a z=%%x-1
				for %%z in (!z!) do if "!M_%%y:~%%z,1!" neq "X" (
					rem 如果边能被看得见就加入列表
					set fs=!fs! !fx1!.!fy1!.!fx1!.!fy2!
				)
			)
			if !fx2! lss 0 (
				set /a z=%%x+1
				for %%z in (!z!) do if "!M_%%y:~%%z,1!" neq "X" (
					set fs=!fs! !fx2!.!fy1!.!fx2!.!fy2!
				)
			)
			if !fy1! gtr 0 (
				set /a z=%%y-1
				for %%z in (!z!) do if "!M_%%z:~%%x,1!" neq "X" (
					set fs=!fs! !fx1!.!fy1!.!fx2!.!fy1!
				)
			)
			if !fy2! lss 0 (
				set /a z=%%y+1
				for %%z in (!z!) do if "!M_%%z:~%%x,1!" neq "X" (
					set fs=!fs! !fx1!.!fy2!.!fx2!.!fy2!
				)
			)
		)
	)
)

for %%f in (!fs!) do (
	for /f "tokens=1-4 delims=." %%a in ("%%f") do (
		set /a fx1=%%a,fy1=%%b,fx2=%%c,fy2=%%d
		rem 旋转边
		set /a "lx1=(fx1*P_cos+fy1*P_sin)/100,ly1=(fx1*P_sin-fy1*P_cos)/100"
		set /a "lx2=(fx2*P_cos+fy2*P_sin)/100,ly2=(fx2*P_sin-fy2*P_cos)/100"

		set no=0
		if !ly1! lss 10 if !ly2! lss 10 set no=1
		if !no! equ 0 (
			rem 剪裁边
			if !ly1! lss 1 set /a "lx1=lx2-(lx2-lx1)*(ly2-1)/(ly2-ly1),ly1=1"
			if !ly2! lss 1 set /a "lx2=lx1-(lx1-lx2)*(ly1-1)/(ly1-ly2),ly2=1"

			rem 透视除法
			set /a x1=SW_2+SW*lx1/ly1/4,x2=SW_2+SW*lx2/ly2/4
			set /a y1=SH_2+SH_2*100/ly1/4,y2=SH_2+SH_2*100/ly2/4

			if !x1! gtr !x2! (
				set /a t=x1,x1=x2,x2=t
				set /a t=y1,y1=y2,y2=t
			)

			rem 绘制范围
			if !x1! lss 0 (set dx1=0) else set dx1=!x1!
			if !x2! gtr !SW1! (set dx2=!SW1!) else set dx2=!x2!

			rem 绘制产生的直角梯形
			if !dx1! lss !dx2! for /l %%x in (!dx1!,1,!dx2!) do (
				rem 最偷懒的地方:如果每一列最底下边的纵坐标y小于已绘制的纵坐标则放弃绘制
				set /a "y=y1-(y1-y2)*(x1-%%x)/(x1-x2)"
				if !SCY_%%x! leq !y! (
					rem 这里不直接绘制,而是生成一个与横坐标有关的数组
					rem 到以后再一遍绘制提升效率
					set SCY_%%x=!y!
					set "ch=|"
					if %%x neq !x1! if %%x neq !x2! set "ch=-"
					set SCC_%%x=!ch!
				)
			)
		)
	)
)

rem 初始化地图的下半部分
set SC_%SH_2%=
for /l %%x in (0,1,%SW1%) do set "SC_%SH_2%=!SC_%SH_2%! "
for /l %%y in (%SH_2%,1,%SH%) do set "SC_%%y=!SC_%SH_2%!"

rem 根据这个与横坐标有关的数组进行绘制
for /l %%x in (0,1,%SW1%) do (
	set /a z=%%x+1
	for %%z in (!z!) do (
		if "!SCC_%%x!"=="|" (
			for /l %%y in (%SH_2%,1,!SCY_%%x!) do (
				set "SC_%%y=!SC_%%y:~0,%%x!|!SC_%%y:~%%z!"
			)
		) else if "!SCC_%%x!"=="-" (
			for %%y in (!SCY_%%x!) do (
				set "SC_%%y=!SC_%%y:~0,%%x!-!SC_%%y:~%%z!"
			)
		)
	)
)

rem 垂直翻转,偷懒本性暴露无遗(^_^)
for /l %%y in (%SH_2%,1,%SH%) do (
	set /a y=SH-%%y
	set SC_!y!=!SC_%%y!
)

rem 绘制三角形
for /l %%s in (1,1,20) do if defined S_%%s_x (
	set /a fx=S_%%s_x-P_x,fy=S_%%s_y-P_y
	set /a "lx=(fx*P_cos+fy*P_sin)/100,ly=(fx*P_sin-fy*P_cos)/100"
	if !ly! gtr 20 if !ly! lss 400 (
		set /a "x1=SW_2+SW*(lx-20)/ly/4,x2=SW_2+SW*(lx+20)/ly/4"
		set /a "y=SH_2+SH_2*100/ly/4,x=(x1+x2)/2"
		if !x1! geq 0 if !x2! lss !SW! (
			for /l %%x in (!x1!,1,!x2!) do if !SCY_%%x! leq !y! (
				set /a x0=%%x+1
				if %%x leq !x! (
					set /a "y1=y-(%%x-x1)*2"
				) else (
					set /a "y1=y-(x2-%%x)*2"
				)
				set /a y2=y1-1
				for /f "tokens=1-2" %%y in ("!y! !x0!") do (
					set "SC_%%y=!SC_%%y:~0,%%x!_!SC_%%y:~%%z!"
				)
				for %%z in (!x0!) do (
					for /l %%y in (!y2!,1,!y1!) do (
						set "SC_%%y=!SC_%%y:~0,%%x!|!SC_%%y:~%%z!"
					)
				)
			)
		)
	)
)

set "SC_24=!SC_24:~0,29!|!SC_24:~30!"
set "SC_25=!SC_25:~0,28!-!SC_25:~29,1!-!SC_25:~31!"
set "SC_26=!SC_26:~0,29!|!SC_26:~30!"

rem 颜色变化
if !CD_SHOOT! equ 1 (
	if !B_SHOOT! gtr 0 (
		color 6E
	) else (
		color 9B
	)
) else if !CD_SPIKE! gtr 1 (
	color CE
) else (
	color F
)

set SC=
for /l %%y in (0,1,%SH%) do set SC=!SC!!SC_%%y!

set pa=___!P_l!
set pb=___!P_k!
set "SC_LN=|                                      |"

rem 无脑显示数字
set "SC=!SC!___________________________________________________________"
set "SC=!SC!HOPE      !SC_LN!DISTROYED"
set "SC=!SC!!Znum%pa:~-3,1%_1!!Znum%pa:~-2,1%_1!!Znum%pa:~-1%_1! !SC_LN!!Znum%pb:~-3,1%_1!!Znum%pb:~-2,1%_1!!Znum%pb:~-1%_1!"
set "SC=!SC!!Znum%pa:~-3,1%_2!!Znum%pa:~-2,1%_2!!Znum%pa:~-1%_2! !SC_LN!!Znum%pb:~-3,1%_2!!Znum%pb:~-2,1%_2!!Znum%pb:~-1%_2!"
set "SC=!SC!!Znum%pa:~-3,1%_3!!Znum%pa:~-2,1%_3!!Znum%pa:~-1%_3! !SC_LN!!Znum%pb:~-3,1%_3!!Znum%pb:~-2,1%_3!!Znum%pb:~-1%_3!"
set "SC=!SC!!Znum%pa:~-3,1%_4!!Znum%pa:~-2,1%_4!!Znum%pa:~-1%_4!%%!SC_LN!!Znum%pb:~-3,1%_4!!Znum%pb:~-2,1%_4!!Znum%pb:~-1%_4!"
set "SC=!SC!!Znum%pa:~-3,1%_5!!Znum%pa:~-2,1%_5!!Znum%pa:~-1%_5! !SC_LN!!Znum%pb:~-3,1%_5!!Znum%pb:~-2,1%_5!!Znum%pb:~-1%_5!"

cls & echo(!SC!

set SC=
goto loop
