(
@echo off
rem 禁用变量拓展延迟以防止路径中有!字符
setlocal disabledelayedexpansion
rem 获取hta的绝对路径
rem 启动hta
for %%a in ("%~dp0ctrl2.hta") do start mshta "%%~fa" %1
rem 等待临时文件创建
if "%1" neq "" call :s "%1"
exit /b
)
:s
(
if not exist %1 goto s
goto :eof
)